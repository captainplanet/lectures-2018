#include <iostream>
using namespace std;

//Global variable
//Placed in static memory
int global_var;

int main()
{
	//Local variable	
	//Placed in automatic memory (stack)
	int local_var;

	int size = 1024*1024;
	for (int i = 0; i < 300; ++i)
	{
		//Dynamic variable
		//Placed in dynamic memory (heap)
		int* dynamic_var = new int[size];
		//Some actions
		//dynamic_var[50] = 100500; //*(dynamic_var+50)

		//Delete dynamic variable
		delete[] dynamic_var;
	}


	

	system("pause");
	return 0;
}