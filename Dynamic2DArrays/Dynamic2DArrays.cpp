#include <iostream>
using namespace std;

int main()
{
	int rows;
	int cols;
	cout << "Enter rows and cols: ";
	cin >> rows >> cols;
	//Initialization
	int** matrix = new int*[rows];
	for (int i = 0; i < rows; ++i)
		matrix[i] = new int[cols];

	//Logic
	for (int row = 0; row < rows; ++row)
		for (int col = 0; col < cols; ++col)
			matrix[row][col] = row*cols + col;

	//Output
	for (int row = 0; row < rows; ++row)
	{
		for (int col = 0; col < cols; ++col)
			cout << matrix[row][col] << '\t';
		cout << endl;
	}

	//Cleaning the memory
	for (int i = 0; i < rows; ++i)
		delete[] matrix[i];
	delete[] matrix;


	system("pause");
	return 0;
}