#include <iostream>
#include <math.h>
using namespace std;

int main()
{
	int size;
	cout << "Enter size: ";
	cin >> size;
	//Iterating through rows
	for (int row = 1; row <= size; ++row)
	{
		//MAGIC NUMBERS!
		double half = (size + 1) / 2.0;
		int stars = 2 * (half - abs(half - row)) - 1;
		//Iterating through symbols in the row
		for (int col = 0; col < (size + stars) / 2; ++col)
			cout << (col < (size - stars) / 2 ? " " : "*");
		cout << endl;
	}
	system("pause");
	return 0;
}