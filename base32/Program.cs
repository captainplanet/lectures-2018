﻿using System;
using System.IO;
using SimpleBase;

namespace base32
{
    class Program
    {
        static void Main(string[] args)
        {
            var str = File.ReadAllText("message.txt");
            str = str.Replace("\r\n", "");
            var cnt = str.Length / 8;
            var res = Base32.Crockford.Decode(str);

            Console.WriteLine();
            Console.WriteLine(res);
            Console.Read();
        }
    }
}
