#include <iostream>
#include <time.h>
using namespace std;

//Function allocates memory
template <typename Type>
Type* createArray(int size)
{
	return new Type[size];
}

//Function assigns random values to array
void randomArray(double sourceArray[], int size)
{
	for (int i = 0; i < size; ++i)
		sourceArray[i] = 10 * ((double)rand() / RAND_MAX);
}

//Function returns max element of the input array
double findMaxElem(const double sourceArray[], int size)
{
	double maxElem = sourceArray[0];
	for (int i = 1; i < size; ++i)
		if (sourceArray[i] > maxElem)
			maxElem = sourceArray[i];
	return maxElem;
}

//Function outputs array to console
void printArray(const double sourceArray[], int size)
{
	for (int i = 0; i < size; ++i)
		cout << sourceArray[i] << "\t";
	cout << endl;
}

//Function cleans array memory
void cleanArray(double sourceArray[])
{
	delete[] sourceArray;
}

void initData(double* &sourceArray, int &size)
{
	srand(time(NULL));

	cout << "Input array size: ";
	cin >> size;
	sourceArray = createArray<double>(size);
	randomArray(sourceArray, size);
}

int main()
{
	//Variables
	int size;
	double maxElem;
	double *randArray;

	//Initialization
	initData(randArray, size);

	//Logic
	maxElem = findMaxElem(randArray, size);

	//Output
	printArray(randArray, size);
	cout << "Max elem: " << maxElem << endl;

	//Cleaning
	cleanArray(randArray);

	system("pause");
	return 0;
}