#include <iostream>
#include <time.h>
using namespace std;

int mainOld()
{
	//Variables
	int size;
	double maxElem;
	double *randArray;

	//Initialization
	srand(time(NULL));

	cout << "Input array size: ";
	cin >> size;
	randArray = new double[size];
	for (int i = 0; i < size; ++i)
		randArray[i] = 10 * ((double)rand()/RAND_MAX);

	//Logic
	maxElem = randArray[0];
	for (int i = 1; i < size; ++i)
		if (randArray[i] > maxElem)
			maxElem = randArray[i];

	//Output
	for (int i = 0; i < size; ++i)
		cout << randArray[i] << "\t";
	cout << endl;

	cout << "Max elem: " << maxElem << endl;

	//Cleaning
	delete[] randArray;

	system("pause");
	return 0;
}