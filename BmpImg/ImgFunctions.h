#include "BmpStructures.h"
#include <fstream>
using namespace std;

int ReadRgbImage(char filename[], RgbImg& img);
int WriteRgbImg(char filename[], const RgbImg& img);
void ClearImgMemory(RgbImg& img);
void BlurImg(RgbImg& img, int kernel = 1);
void MedianFilterImg(RgbImg& img, int kernel = 1);
void InsertionSort(BYTE* array, int size);
void RgbToGray(RgbImg& img);