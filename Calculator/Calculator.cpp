#include <iostream>
using namespace std;

int main()
{
	double left;
	double right;
	double result;
	char op;

	while (true)
	{
		cout << "Input expression: ";
		cin >> left >> op >> right;
		switch (op)
		{
			case '+':
			{
				result = right + left;
				break;
			}
			case '-':
			{
				result = left - right;
				break;
			}
			case '*':
			{
				result = right * left;
				break;
			}
			case '/':
			{
				result = left / right;
				break;
			}
			default:
			{
				cout << "Incorrect operator" << endl;
				system("pause");
				return -1;
			}
		}
		cout << left << op << right << " = " << result << endl;

		cout << "Continue? y/n" << endl;
		char answer;
		cin >> answer;
		if (answer != 'y')
			break;
	}
	//system("pause");
	return 0;
}