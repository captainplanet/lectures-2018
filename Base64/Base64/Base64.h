#pragma once

char* base64_encode(char message[], int messageSize);
char* base64_decode(char encoded[], int encodedSize, int& decodedSize);