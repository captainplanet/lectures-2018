#include <iostream>
#include <fstream>
#include "Base64.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Ru");
	//Входной файл
	ifstream in;
	in.open("image.png", ios::binary);
	if (!in.is_open())
	{
		cout << "Input file error!" << endl;
		system("pause");
		return 0;
	}
	//Узнаем размер файла
	in.seekg(0, ios::end);
	int fSize = in.tellg();

	cout << "Размер исходного сообщения: ";
	cout << fSize << endl << endl;

	//Возвращаемся в начало файла
	in.seekg(0, ios::beg);

	//Считываем все в память
	char* fileContent = new char[fSize];
	in.read(fileContent, fSize);

	//Кодируем данные в BASE64
	int size;
	char* decoded = base64_decode(fileContent, fSize, size);
	

	cout << "Размер закодированного сообщения: ";
	cout << size << endl << endl;

	//Выходной файл
	ofstream out;
	out.open("out.png", ios::binary);
	if (!out.is_open())
	{
		cout << "Output file error!" << endl;
		system("pause");
		return 0;
	}
	out.write(decoded, size);

	//Закрываем файлы
	out.close();
	in.close();

	//Очищаем память
	delete[] fileContent;
	delete[] decoded;	
	//int size;
	//char* encoded = base64_encode("1234", 4);
	//char* decoded = base64_decode(encoded, strlen(encoded), size);
	//cout << encoded << endl;
	//cout << decoded << endl;

	system("pause");
	return 0;
}