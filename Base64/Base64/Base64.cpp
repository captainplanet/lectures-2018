#include "Base64.h"
#include <iostream>
using namespace std;
char base64_chars[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

char _64bASE_chars[] =
"0123456789"
"abcdefghijklmnopqrstuvwxyz"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"+/";

char* base64_encode(char message[], int messageSize)
{
	//Считаем размер закодированной строки
	int encodedSize = 4 * (messageSize / 3)
				+ 4 * ((bool)(messageSize % 3)) + 1;

	//Выделяем память
	char* encodedStr = new char[encodedSize];

	//Делаем строку в C-стиле
	encodedStr[encodedSize - 1] = '\0';

	//Буфер для входных символов
	unsigned char source[3];

	//Индексы символов выходной последовательности
	unsigned char indices[4];

	//Счетчик исходных символов
	int counter = messageSize;
	
	//Индекс для входного буфера
	int idx = 0;

	//Индекс для выходного сообщения
	int encodedStrIdx = 0;

	//Обработка основной части
	while (counter--)
	{
		source[idx++] = *(message++);
		//Обрабатываем каждые три подряд идущих символа
		if (idx == 3)
		{
			idx = 0;

			//Разбиение на 4 шестибитных индекса
			indices[0] = source[0] >> 2;
			indices[1] = ((source[0] & 3) << 4)
						+ (source[1] >> 4);
			indices[2] = ((source[1] & 15) << 2)
							+ (source[2] >> 6);	
			indices[3] = source[2] & 63;

			//В выходную строку записываем символы по полученным индексам
			for (int i = 0; i < 4; ++i)
				encodedStr[encodedStrIdx++] = base64_chars[indices[i]];
		}
	}

	//Если остались необработанные символы
	if (idx)
	{
		//Добиваем до тройки нулями
		for (int j = idx; j < 3; ++j)
			source[j] = '\0';

		//Считаем индексы символов
		indices[0] = source[0] >> 2;
		indices[1] = ((source[0] & 3) << 4)
			+ (source[1] >> 4);
		indices[2] = ((source[1] & 15) << 2)
			+ (source[2] >> 6);
		indices[3] = source[2] & 63;

		//Записываем значащие символы
		for (int i=0; i<idx + 1;++i)
			encodedStr[encodedStrIdx++] = base64_chars[indices[i]];

		//Оставшиеся забиваем символами '='
		while (idx++ < 3)
			encodedStr[encodedStrIdx++] = '=';
	}

	return encodedStr;
}

int findIdx(char val)
{
	for (int i = 0; i < 64; ++i)
		if (val == _64bASE_chars[i])
			return i;
	return -1;
}

char* base64_decode(char encoded[], int encodedSize, int& decodedSize)
{
	int sizeWithoutInvalidChars = encodedSize;
	for (int i = 0; i < encodedSize; ++i)
	{
		int c_idx = findIdx(encoded[i]);
		if (encoded[i] != '=' && c_idx == -1)
		{
			sizeWithoutInvalidChars--;
			//cout << (int)encoded[i] << endl;
		}
	}
		
	//Считаем размер закодированной строки
	decodedSize = 3 * (sizeWithoutInvalidChars / 4) + 1;
	int padCount = 0;
	while (encoded[encodedSize - 1 - padCount] == '=')
		++padCount;
	decodedSize -= padCount;
	
	//Выделяем память
	char* decodedStr = new char[decodedSize];
	decodedStr[decodedSize - 1] = '\0';
	//Буфер для входных символов
	unsigned char source[3];

	//Индексы символов выходной последовательности
	unsigned char indices[4];

	//Счетчик исходных символов
	int counter = encodedSize;

	//Индекс для входного буфера
	int idx = 0;

	//Индекс для выходного сообщения
	int decodedStrIdx = 0;

	//Обработка основной части
	while (counter--)
	{
		char curr = *(encoded++);
		int c_idx = findIdx(curr);
		
		if (curr == '=')
			indices[idx++] = 0;
		else
		{
			if (c_idx == -1)
				continue;
			indices[idx++] = c_idx;
		}
			

		//Обрабатываем каждые три подряд идущих символа
		if (idx == 4)
		{
			idx = 0;

			//Разбиение на 4 шестибитных индекса
			source[0] = (indices[0] << 2) + (indices[1] >> 4);
			source[1] = (indices[1] << 4) + (indices[2] >> 2);
			source[2] = (indices[2] << 6) + indices[3];

			//В выходную строку записываем символы по полученным индексам
			for (int i = 0; i < 3 - (counter == 0) * padCount; ++i)
				decodedStr[decodedStrIdx++] = source[i];
		}
	}

	return decodedStr;
}
