#include <iostream>
using namespace std;

template <typename Type>
class List
{
private:
	struct Node
	{
		Type data;
		Node* next;
		Node(const Type& _data) : data(_data), next(nullptr) {}
	};
	
	Node* first;
	Node* last;
	unsigned int count;

public:
	List() : first(nullptr), last(nullptr), count(0) {}
	List(const List& source);
	~List() { clear(); }
	void clear();
	unsigned int size() const { return count; };
	void push_back(const Type& data);
	void push_front(const Type& data);
	Type& operator[](unsigned int idx) const;

	class Iterator
	{
	private:
		Node* current;
	public:
		Iterator(Node* curr = nullptr) : current(curr) {}
		Type& operator*() const { return current->data; }
		bool operator==(const Iterator& right) { return current == right.current; }
		bool operator!=(const Iterator& right) { return current != right.current; }
		Iterator& operator++()
		{
			if (!current) throw "Iterator out of bounds";
			current = current->next;
			return *this;
		}

		Iterator operator++(int n)
		{
			if (!current) throw "Iterator out of bounds";
			Node* tmp = current;
			current = current->next;
			return Iterator(tmp);
		}
	};

	Iterator begin() const { return Iterator(first); }
	Iterator end() const { return Iterator(); }
	Iterator find(const Type& data) const;
};

template <typename Type>
typename List<Type>::Iterator List<Type>::find(const Type& data) const
{
	Node* current = first;
	while (current)
	{
		if (current->data == data) return Iterator(current);
		current = current->next;
	}
	return end();

	/*
	//�� �� ����� ����� ���������
	for (auto curr = begin(); curr != end(); ++curr)
		if (*curr == data) return curr;
	return end();
	*/
}

template <typename Type>
void List<Type>::push_back(const Type& data)
{
	if (!first)
	{
		first = last = new Node(data);
	}
	else
	{
		//(*last).next
		last->next = new Node(data);
		last = last->next;
	}
	++count;
}

template <typename Type>
void List<Type>::push_front(const Type& data)
{
	if (!first)
	{
		first = last = new Node(data);
	}
	else
	{
		Node* tmp = new Node(data);
		tmp->next = first;
		first = tmp;

		//Node* tmp = first;
		//first = new Node(data);
		//first->next = tmp;
	}
	++count;
}

template<typename Type>
Type & List<Type>::operator[](unsigned int idx) const
{
	if (idx < 0 || idx > count - 1)
		throw "Index out of bounds";
	Node* tmp = first;
	for (int i = 0; i < idx; ++i)
	{
		tmp = tmp->next;
	}

	return tmp->data;
}

template <typename Type>
void List<Type>::clear()
{
	Node* tmp;
	while (first)
	{
		tmp = first;
		first = first->next;
		delete tmp;
	}
	count = 0;
	last = nullptr;
}

template <typename Type>
List<Type>::List(const List<Type>& source)
{
	first = last = nullptr;
	count = 0;

	Node* tmp = source.first;
	while (tmp)
	{
		push_back(tmp->data);
		tmp = tmp->next;
	}
}

int main()
{
	List<int> listInt;
	listInt.push_back(1);
	listInt.push_back(2);
	listInt.push_front(3);
	for (int i = 0; i < listInt.size(); ++i)
		cout << listInt[i] << endl;

	
	for (auto current = listInt.begin(); current != listInt.end(); ++current)
		cout << *current << endl;

	auto foundElem = listInt.find(1);
	if (foundElem != listInt.end())
		cout << *foundElem << " " << *(++foundElem) << endl;

	system("pause");
	return 0;
}