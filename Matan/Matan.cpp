#include <iostream>
#include <math.h>
#include <stdarg.h>
using namespace std;

double f1(double x)
{
	return x*x;
}

double f2(double x)
{
	return sin(x);
}

double rectInt(double(*func)(double), double a, double b, int count)
{
	double dx = (b - a) / count;
	double result = 0;
	while (a < b)
	{
		result += func(a + dx / 2)*dx;
		a += dx;
	}

	return result;
}

double trapInt(double(*func)(double), double a, double b, int count)
{
	double dx = (b - a) / count;
	double result = 0;
	while (a < b)
	{
		result += (func(a)+func(a+dx))*dx/2;
		a += dx;
	}

	return result;
}

int sum(int count, int x, ...)
{
	va_list args;
	va_start(args, x);
	int result = x;
	for (int i = 1; i < count; ++i)
		result += va_arg(args, int);
	va_end(args);
	return result;
}

double prod(double x, ...)
{
	va_list args;
	va_start(args, x);
	double result = 1;
	while (x != 0)
	{
		result *= x;
		x = va_arg(args, double);
	}
	va_end(args);
	return result;
}


int main()
{
	double a;
	double b;
	int count;
	cout << "Enter params: ";
	cin >> a >> b >> count;
	cout << rectInt(f2, a, b, count) << endl;
	cout << trapInt(f2, a, b, count) << endl;

	printf("Int value: %d, double value: %f\n", 10, 1.5);
	cout << sum(3, 5, 8, 10) << endl;
	cout << prod(10,20,0.5,0) << endl;

	system("pause");
	return 0;
}