#ifndef FUNCTIONS_H
#define FUNCTIONS_H
int powerIter(int val, int deg);
double powerIter(double val, double deg);
int counter();
int powerRecursive(int val, int deg);
void hanoy(int size, int from = 1, int to = 2);
#endif