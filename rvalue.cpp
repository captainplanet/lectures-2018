#include <iostream>
using namespace std;

struct Pair {
	int* arr;
	int size;
};

class TestClass {
	Pair p;
public:
	void TestMethod(Pair& t) {
		p.size = t.size;
		p.arr = new int[p.size];
		for (int i = 0; i < p.size; ++i)
			p.arr[i] = t.arr[i];
	}
	void TestMethod(Pair&& t) {
		p.arr = t.arr;
		p.size = t.size;
	}
};

int main() {
	int* x = new int[3];

	Pair p = { x,3 };
	TestClass t;
	t.TestMethod({ x,3 });

	delete[] x;
	system("pause");
	return 0;
}