#include <iostream>
using namespace std;

int main()
{	
	int var = 10;
	int* pointer = &(var);
	cout << pointer << endl; 
	cout << sizeof(pointer) << endl;
	*(pointer) = 256;
	cout << var << endl;
	unsigned char* byte_ptr = (unsigned char*)&var;
	cout << (int)(*(++byte_ptr)) << endl;

	unsigned char* byte0 = (unsigned char*)&var;
	unsigned char* byte1 = (unsigned char*)&var + 1;
	unsigned char* byte2 = (unsigned char*)&var + 2;
	unsigned char* byte3 = (unsigned char*)&var + 3;
	
	int result = 0;
	int power = 1;
	for (int i = 0; i < 4; ++i)
	{
		result += power * (*((unsigned char*)&var + i));
		power *= 256;
	}
	cout << result << endl;
	//Equivalent
	result = (*byte0) + (*byte1 * 256) + (*byte2 * 256 * 256) + 
		(*byte3 * 256 * 256 * 256);
	cout << result << endl;

	result = 0;
	for (int i = 0; i < 4; ++i)
	{
		unsigned char curr_cell= (*((unsigned char*)&var + i));
		result += curr_cell << 8 * i;
	}
	cout << result << endl;

	result = *((int*)byte0);
	cout << result << endl;

	int** p_pointer = &pointer;
	int const_val = 42;
	int const * const p1 = &const_val;

	int arr[5] = { 2, 4, 6, 8, 10 };
	arr[2] = 28; // *(arr+2) = 28;
	int arr2[2];

	system("pause");
	return 0;
}