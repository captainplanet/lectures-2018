#include <iostream>
using namespace std;

int main()
{
	int count;
	double sum_while = 0;
	double sum_do_while = 0;
	double sum_for = 0;
	int curr_elem = 0;
	double max_val;
	cout << "Input max val: ";
	cin >> max_val;

	while (sum_while < max_val)
		sum_while += 1.0 / ++curr_elem;

	cout << "Elements count (WHILE) = " << curr_elem << endl;
	curr_elem = 0;

	////���� �����, ����� ���� �� ���� ��������
	////����������� ����������
	do sum_do_while += 1.0 / ++curr_elem;
	while (sum_do_while < max_val);
	cout << "Elements count (DO-WHILE) = " << curr_elem << endl;

	cout << "Input elements count: ";
	cin >> count;

	for (curr_elem = 1;
		curr_elem <= count;
		++curr_elem)
		sum_for += 1.0 / curr_elem;
	cout << "SUM FOR = " << sum_for << endl;

	system("pause");
	return 0;
}