#include <iostream>
#include <exception>
#include <fstream>
using namespace std;

void countSymbols(char filePath[], int& ucCount, int& lcCount, int& sCount)
{
	ucCount = lcCount = sCount = 0;
	ifstream in;
	in.open(filePath);
	if (!in.is_open())
	{
		throw runtime_error("Failed to open file");
	}

	//alternative way
	//will ignore EOF at the and
	while (in.peek() != EOF)
	//Allows to read EOF
	//while (!in.eof())
	{
		char curr = in.get();
		if (curr >= 65 && curr <= 90) ++ucCount;
		if (curr >= 97 && curr <= 122) ++lcCount;
		++sCount;
		cout << (int)curr << " " << curr << endl;
	}
	in.close();
}

void countSymbolsString(char filePath[], int& ucCount, int& lcCount, int& sCount)
{
	char buffer[256];
	ucCount = lcCount = sCount = 0;
	ifstream in;
	in.open(filePath);
	if (!in.is_open())
	{
		throw runtime_error("Failed to open file");
	}

	//alternative way
	//will ignore EOF at the and
	char e = in.peek();
	while (e != EOF)
		//Allows to read EOF
		//while (!in.eof())
	{
		//char curr = in.get();
		in.get(buffer, 256);

		//getline skips '\n' at the end
		//in.getline(buffer, 256);
		int bufferSize = strlen(buffer);
		for (int i = 0; i < bufferSize; ++i)
		{
			char curr = buffer[i];
			if (curr >= 65 && curr <= 90) ++ucCount;
			if (curr >= 97 && curr <= 122) ++lcCount;
			++sCount;
			cout << (int)curr << " " << curr << endl;
		}
		//in.get();
		++sCount;
		e = in.peek();
	}
	in.close();
}

int main(int argc, char* argv[])
{
	//for (int i = 0; i < argc; ++i)
	//	cout << argv[i] << endl;

	int upperCaseCount = 0;
	int lowerCaseCount = 0;
	int symbolCount = 0;

	try
	{
		char filePath[] = "in.txt";
		countSymbolsString(filePath, upperCaseCount, lowerCaseCount, symbolCount);
	}
	catch (runtime_error error)
	{
		cout << "Exception caught: " << error.what() << endl;
		system("pause");
		return -1;
	}

	cout << "Symbols count: " << symbolCount << endl;
	cout << "Lower case count: " << lowerCaseCount << endl;
	cout << "Upper case count: " << upperCaseCount << endl;

	system("pause");
	return 0;
}