#include <iostream>
using namespace std;

template <typename Type>
class BinaryTree
{
private:
	struct Node
	{
		Node* parent;
		Node* left;
		Node* right;
		Type data;

		Node(Type _data) :  left(nullptr),
							right(nullptr),
							parent(nullptr),
							data(_data) {}
	};
	Node* root;
	int size;

	void addItemSubtree(const Type& item, Node* subroot);
	void printSubtree(Node* subroot) const;
	void deleteSubtree(Node* subroot);

public:
	BinaryTree() : root(nullptr), size(0) {}
	~BinaryTree()
	{
		if (!root) return;
		deleteSubtree(root);
	}
	int getSize() const { return size; }
	void insert(const Type& data);
	void print() const
	{
		if (!root) return;
		printSubtree(root);
	}
};

template <typename Type>
void BinaryTree<Type>::printSubtree(Node* subroot) const
{
	if (subroot->left) printSubtree(subroot->left);
	cout << subroot->data << endl;
	if (subroot->right) printSubtree(subroot->right);
}

template <typename Type>
void BinaryTree<Type>::deleteSubtree(Node* subroot)
{
	if (subroot->left) deleteSubtree(subroot->left);
	if (subroot->right) deleteSubtree(subroot->right);
	delete subroot;
}

template <typename Type>
void BinaryTree<Type>::insert(const Type& data)
{
	if (!root)
	{
		root = new Node(data);
		++size;
		return;
	}
	addItemSubtree(data, root);
}

template <typename Type>
void BinaryTree<Type>::addItemSubtree(const Type& item, Node* subtree)
{
	if (item > subtree->data)
	{
		if (!subtree->right)
		{
			subtree->right = new Node(item);
			++size;
			return;
		}
		addItemSubtree(item, subtree->right);
	}
	else
	{
		if (!subtree->left)
		{
			subtree->left = new Node(item);
			++size;
			return;
		}
		addItemSubtree(item, subtree->left);
	}
}



int main()
{
	BinaryTree<int> tree;
	tree.insert(5);
	tree.insert(3);
	tree.insert(1);
	tree.insert(2);
	tree.insert(7);
	tree.insert(6);
	tree.insert(8);
	tree.print();

	system("pause");
	return 0;
}