#include <iostream>
#include <fstream>
using namespace std;


int main()
{
	ifstream in;
	in.open("in.txt");
	//in.open("/Users/username/Desktop/../in.txt");
	if (!in.is_open())
	{
		cout << "Failed to open in file" << endl;
		system("pause");
		return -1;
	}

	ofstream out;
	out.open("out.txt");
	if (!out.is_open())
	{
		cout << "Failed to open out file" << endl;
		system("pause");
		return -1;
	}

	int rows;
	int cols;
	in >> rows >> cols;
	int** matrix = new int*[rows];
	for (int currRow = 0; currRow < rows; ++currRow)
		matrix[currRow] = new int[cols];

	for (int currRow = 0; currRow < rows; ++currRow)
		for (int currCol = 0; currCol < cols; ++currCol)
			in >> matrix[currRow][currCol];

	out << rows << " " << cols << endl;
	for (int currRow = 0; currRow < rows; ++currRow)
	{
		for (int currCol = 0; currCol < cols; ++currCol)
			out << matrix[currRow][currCol] << " ";
		out << endl;
	}
	
	in.close();
	out.close();
	for (int i = 0; i < rows; ++i)
		delete[] matrix[i];
	delete[] matrix;
	system("pause");
	return 0;
}