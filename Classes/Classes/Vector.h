#pragma once
#include <iostream>
class Vector;
class TestFriend;


class Vector
{
public:
	// ����������� �� ���������
	Vector();
	// ��� ���� �����������
	Vector(int _size);
	// ��� ���� �����������
	Vector(int source[], int _size);
	//���������� �����������
	Vector(const Vector& source);
	~Vector();

	void print() const;
	Vector& push_back(int val);
	// getting last element
	int pop_back();
	// getting elemnt by index
	int& getElemAt(int index) const;
	int getSize() const;
	static int getObjCount();
	friend std::ostream& operator<<(std::ostream&, const Vector&);
	friend void TestFriend::testMethod(Vector&);
	//friend TestFriend;
	
private:
	int* data;
	int size; // amount of elements
	int capacity; // amount of memory
	static int objCount;
};

class TestFriend
{
public:
	void testMethod(Vector& vec)
	{
		vec.size = 10;
	}
};