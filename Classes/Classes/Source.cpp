#include <iostream>
#include "Vector.h"
using namespace std;

ostream& operator<<(ostream& out, const Vector& vec)
{
	for (int i = 0; i < vec.size; i++)
		out << vec.data[i] << " ";
	out << endl;
	return out;
}

int main()
{
	int* arr = new int[5]{ 1, 2, 3, 4, 5 };
	Vector vec_1(arr, 5);
	int size_1 = vec_1.getSize();	// �������� ������ vec_1
	vec_1.print();				// 1 2 3 4 5

	int size_2 = 10;
	Vector vec_2(size_2);

	// ��������� ������
	for (int i = 0; i < size_2; i++)
		vec_2.push_back(i + 1);

	//������� ������ �� �������
	for (int i = 0; i < size_2; i++)
		cout << vec_2.getElemAt(i) << " " << endl;

	vec_2.push_back(111);	// 1 2 3 4 5 6 7 8 9 10 111


	Vector vecToCopyTo = vec_2;

	Vector vec_3;

	// ��� ���� ������ ���������� �������
	for (int i = 0; i < 10; i++)
		vec_3.push_back(i + 1);

	vec_3.print(); 		// 1 2 3 4 5 6 7 8 9 10

	//cout << vec_3 << endl;

	//������������ �������� �������
	Vector* vec = new Vector(arr, 5);
	vec->push_back(1).push_back(2).push_back(5);
	
	// HACKERMAN!!11
	((int*)((int*)vec)[0])[0] = 3;
	cout << sizeof(Vector) << endl;
	//vec->print();
	cout << *vec;
	
	delete vec;

	//cout << vec->objCount << endl;
	//cout << Vector::objCount << endl;

	cout << vec->getObjCount() << endl;
	cout << Vector::getObjCount() << endl;

	system("pause");
	return 0;
}