#include "tools.h"
#include <iostream>
#include <chrono>
#include <cstring>

using namespace std;
using namespace std::chrono;

//Пример глобальной переменной
int globalX = 5;

static high_resolution_clock::time_point sstart;
void tic()
{
	sstart = high_resolution_clock::now();
}

long toc()
{
	return duration_cast<milliseconds>(high_resolution_clock::now() - sstart).count();
}

void printArray(const int source[], int size)
{
	for (int i = 0; i < size; ++i)
		cout << source[i] << "\t";
	cout << endl;
}

void randArray(int source[], int size, int maxVal, int minVal)
{
	for (int i = 0; i < size; ++i)
		source[i] = minVal + rand() % (maxVal - minVal + 1);
}

void copyArray(const int source[], int dest[], int size)
{
	for (int i = 0; i < size; ++i)
		dest[i] = source[i];
}

bool strGreater(char strLeft[], char strRight[])
{
	return strcmp(strLeft, strRight) == 1;
}