#include <iostream>
#include <time.h>
#include "sorts.h"
#include "tools.h"
#include <cstring>
#include <algorithm>
#include "radix.h"
#include <functional>
using namespace std;

template <typename Type>
void testSort(char sortName[],
	Type* source,
	Type size,
	void(*sortFunc)(Type*, int, bool(*)(Type, Type)),
	bool(*cmpFunc)(Type, Type) = greater)
{
	int* tmpArr = new int[size];
	long finishToc;
	cout << sortName << endl;
	copyArray(source, tmpArr, size);
	tic();
	sortFunc(tmpArr, size, cmpFunc);
	finishToc = toc();
	cout << "time passed: " << finishToc << endl;
	delete[] tmpArr;
}

int main()
{
	srand(time(NULL));
	//������������� ����������
	const int SIZE = 200000;
	int* arr = new int[SIZE];
	int* sourceArr = new int[SIZE];
	long finishToc;
	clock_t start;
	clock_t finish;

	randArray(arr, SIZE, SIZE);
	copyArray(arr, sourceArr, SIZE);
	
	//������� ��������� ���������� ����� ��������� �� �������
	bubbleSort<int>(arr, SIZE, greaterNum);
	//������� ��������� ���������� ��������������� � ������� ������-���������
	bubbleSort<int>(arr, SIZE, [](int a, int b) { return a > b; });


	////testSort("Bubble sort", arr, SIZE, bubbleSort<int>);
	//testSort("Merge sort", arr, SIZE, mergeSort<int>);
	//testSort("Heap sort", arr, SIZE, heapSort<int>);

	//cout << "Radix sort: " << endl;
	//start = clock();
	//radixSort(arr, SIZE);
	//finish = clock();
	//cout << "Time passed: " << (double)1000*(finish - start) / CLOCKS_PER_SEC << endl;


	//cout << "Bubble sort: " << endl;
	//start = clock();
	//bubbleSort(arr, SIZE);
	//finish = clock();
	//cout << "Time passed: " << (double)1000*(finish - start) / CLOCKS_PER_SEC << endl;

	//cout << "Selection sort: " << endl;
	//copyArray(sourceArr, arr, SIZE);
	//start = clock();
	//selectionSort(arr, SIZE);
	//finish = clock();
	//cout << "Time passed: " << (double)1000 * (finish - start) / CLOCKS_PER_SEC << endl;

	//cout << "Insertion sort: " << endl;
	//copyArray(sourceArr, arr, SIZE);
	//tic();
	//insertionSort(arr, SIZE);
	//finishToc = toc();
	//cout << "Time passed: " << finishToc << endl;

	//cout << "Insertion sort (binary search): " << endl;
	//copyArray(sourceArr, arr, SIZE);
	//tic();
	//insertionSortBinary(arr, SIZE);
	//finishToc = toc();
	//cout << "Time passed: " << finishToc << endl;

	//cout << "Merge sort: " << endl;
	//copyArray(sourceArr, arr, SIZE);
	//tic();
	//mergeSort(arr, SIZE);
	//finishToc = toc();
	//cout << "Time passed: " << finishToc << endl;

	//cout << "Quick sort: " << endl;
	//copyArray(sourceArr, arr, SIZE);
	//tic();
	//qSort(arr, SIZE);
	//finishToc = toc();
	//cout << "Time passed: " << finishToc << endl;

	//cout << "stl sort: " << endl;
	//copyArray(sourceArr, arr, SIZE);
	//tic();
	//sort(arr, arr + SIZE - 1);
	//finishToc = toc();
	//cout << "Time passed: " << finishToc << endl;

	////���������� ������� �����
	//char strArray[4][5] = { "str3", "str1", "str2", "abc" };
	//char** strDynamic = new char*[4];
	//for (int i = 0; i < 4; ++i)
	//	strDynamic[i] = strArray[i];

	//cout << "C-string array: " << endl;
	//for (int i = 0; i < 4; ++i)
	//	cout << strDynamic[i] << endl;

	//bubbleSort(strDynamic, 4, strGreater);

	//cout << "C-string array sorted: " << endl;
	//for (int i = 0; i < 4; ++i)
	//	cout << strDynamic[i] << endl;

	////������� ������
	//delete[] arr;
	//delete[] sourceArr;
	//delete[] strDynamic;

	system("pause");
	return 0;
}