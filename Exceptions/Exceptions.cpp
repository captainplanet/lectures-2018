#include <iostream>
#include <exception>
using namespace std;

void printArray(const int source[], int size)
{
	if (size < 0)
		throw range_error("Negative size");

	for (int i = 0; i < size; ++i)
		cout << source[i] << "\t";
	cout << endl;
}

//return 0 -- success
//return -1 -- negative size error
int printArrayErrorCode(const int source[], int size)
{
	if (size < 0)
		return -1;

	for (int i = 0; i < size; ++i)
		cout << source[i] << "\t";
	cout << endl;

	return 0;
}

int main()
{
	int sourceArray[] = { 1,2,3 };
	int retVal = printArrayErrorCode(sourceArray, -1);
	if (retVal < 0)
	{
		cout << "negative size" << endl;
	}

	try
	{
		printArray(sourceArray, -1);
	}
	catch (exception error)
	{
		cout << "Exception: " << error.what() << endl;
	}
	catch (range_error error)
	{
		cout << "Range error: " << error.what() << endl;
	}

	try
	{
		int testVar = 1;
		if (testVar == 0)
			throw -1;
		if (testVar == 1)
			throw "Error";
		
		cout << "end of try block" << endl;
	}
	catch (int error)
	{
		cout << error << endl;
	}
	catch (const char* str)
	{
		cout << "String err: " << str << endl;
	}
	

	system("pause");
	return 0;
}