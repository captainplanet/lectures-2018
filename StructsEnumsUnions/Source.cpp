#include <iostream>
#include <fstream>
using namespace std;

struct Point
{
	int x;
	int y;
};

#pragma pack(push,1)
struct TestStruct
{
	int x;
	char c;
};
#pragma pack(pop)

struct BitStruct
{
	unsigned int x : 8;
	unsigned int y : 8;
	unsigned int z : 4;
};

union TestUnion
{
	int x;
	double y;
	char z;
};

enum ParameterType { integer = 1, rational = 3, character };

struct Parameter
{
	ParameterType type;
	TestUnion value;
};

enum DayOfWeek 
{
	Monday, 
	Tuesday, 
	Wednesday, 
	Thursday,
	Friday,
	Saturday,
	Sunday
};

char daysConverter[7][16] =
{
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Sunday"
};

void printDayOfWeek1(DayOfWeek day)
{
	switch (day)
	{
		case Monday:
		{
			cout << "Today is Monday" << endl;
			break;
		}
		case Tuesday:
		{
			cout << "Today is Tuesday" << endl;
			break;
		}
		case Wednesday:
		{
			cout << "Today is Wednesday" << endl;
			break;
		}
		case Thursday:
		{
			cout << "Today is thursday" << endl;
			break;
		}
		case Friday:
		{
			cout << "Today is friday" << endl;
			break;
		}
		case Saturday:
		{
			cout << "Today is saturday" << endl;
			break;
		}
		case Sunday:
		{
			cout << "Today is sunday" << endl;
			break;
		}
	}
}

void printDayOfWeek2(DayOfWeek day)
{
	cout << "Today is " << daysConverter[day] << endl;
}

int main()
{
	Point p1;
	p1.x = 10;
	p1.y = 20;
	cout << p1.x << " " << p1.y << endl;

	Point* p_dynamic = new Point;
	(*p_dynamic).x = 2;
	p_dynamic->y = 25;

	cout << "Size of Point: " << endl;
	cout << sizeof(Point) << endl;
	cout << "Size of TestStruct: " << endl;
	cout << sizeof(TestStruct) << endl;

	TestStruct outStruct;
	outStruct.x = 355;
	outStruct.c = 'a';

	ofstream out;
	out.open("out.dat", ios::binary);
	if (!out.is_open())
	{
		cout << "Out file error" << endl;
		system("pause");
		return -1;
	}
	out.write((char*)&outStruct, sizeof(TestStruct));
	out.close();

	TestStruct inStruct;
	ifstream in;
	in.open("out.dat", ios::binary);
	if (!in.is_open())
	{
		cout << "In file error" << endl;
		system("pause");
		return -1;
	}
	in.read((char*)&inStruct, sizeof(TestStruct));
	in.close();
	cout << inStruct.x << " " << inStruct.c << endl;
	cout << "BitStruct size: " << sizeof(BitStruct) << endl;

	BitStruct bS = {1, 2, 3};
	cout << bS.x << " " << bS.y << " " << bS.z << endl;

	printDayOfWeek1(Monday);
	printDayOfWeek2(Sunday);
	printDayOfWeek1((DayOfWeek)0);
	for (int i = 0; i < 7; ++i)
		printDayOfWeek2((DayOfWeek)i);

	system("pause");
	return 0;
}