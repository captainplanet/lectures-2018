#include <iostream>
#include <functional>
using namespace std;

//��� ��������������� �������
double f1(double x)
{
	return x * 2;
}

double f2(double x)
{
	return x*x;
}

//�������, ����������� ��������� ���������� ���� ������ �������
double fSum(double x, double(*func1)(double), double(*func2)(double))
{
	return func1(x) + func2(x);
}

//������� ��������� ������ ��������� � f1 � f2
double fSum(double x)
{
	return f1(x) + f2(x);
}

//������� ���������� ������ -- ��������� ��������� ���� ������� �� �����
function<double(double)> genFunc(function<double(double)> f1, function<double(double)> f2)
{
	return [=](double x) { return f1(x) + f2(x); };
}

//������ ������������� ���������� �������
void testFunc()
{
	//������� �������
	function<double(double)> sumFunc = genFunc(f1, f2);
	double x = 2;
	//��������� ���������� �������
	double y = sumFunc(x);
}

//������ ������ ��� ������������� ��������� �� ������
function<void()> closureFail()
{
	int y = 25;
	return [&y]() { cout << y << endl; };
}

//������������� ����������� �������
void testClosureFail()
{
	auto failFunc = closureFail();
	failFunc();
}

//����������� ���������� -- ����� ������ � ������ �������� �����
static int globalX = 0;

//����������� ���������� ������ �������
void testStatic()
{
	static int x = 0;
	cout << ++x << endl;
}

int main()
{
	system("pause");
	return 0;
}