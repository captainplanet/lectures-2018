#include <iostream>
using namespace std;

int sum(int a, int b)
{
	return a + b;
}


void swap_arg(int a, int b)
{
	int tmp = a;
	a = b;
	b = tmp;
}

void swap_ptr(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void swap_ref(int& a, int& b)
{
	int tmp = a;
	a = b;
	b = tmp;
}

int main()
{
	int megatron9000 = 9000;
	int* ptr = &(megatron9000);
	int& ref = megatron9000;
	
	ref = 9001;
	cout << megatron9000 << endl;

	int arr[5] = { 1,1,2,3,5 };
	int& middle = arr[2];
	middle = 3;
	cout << arr[2] << endl;
	int x = 2;
	int y = 3;
	cout << sum(x, y) << endl;

	cout << "No swap" << endl;
	cout << x << " " << y << endl;
	cout << "Swap arg" << endl;
	swap_arg(x, y);
	cout << x << " " << y << endl;
	cout << "Swap ref" << endl;
	swap_ref(x, y);
	cout << x << " " << y << endl;
	cout << "Swap ptr" << endl;
	swap_ptr(&x, &y);
	cout << x << " " << y << endl;

	system("pause");
	return 0;
}