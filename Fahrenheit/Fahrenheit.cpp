#include <iostream>
using namespace std;

int main()
{
	double fahrenheit = 0;
	double celsius = 0;

	setlocale(LC_ALL, "Ru");
	cout << "������� ����������� ";
	cout << "� �������� ����������: " << endl;

	cin >> fahrenheit;

	celsius = 5.0 / 9 * (fahrenheit - 32);

	cout << "����������� � �������� ";
	cout << "�������: " << celsius << endl;

	if (celsius >= 30)
		cout << "����!" << endl;
	else if (celsius <= 0)
		cout << "��������� ��������" << endl;
	else
		cout << "��!" << endl;
	
	//������ ������������ if ��� ������
	//celsius >= 30 ? cout << "����!" : cout << "�������";
	
	//������ ������������ if 
	//��� ������������� ���������� ������
	//int state = celsius >= 30 ? 1 : -1;

	system("pause");
	return 0;
}