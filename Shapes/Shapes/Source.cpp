#include <iostream>
using namespace std;

class Shape
{
public:
	Shape() { cout << "Shape was created" << endl; }
	virtual ~Shape() { cout << "Shape was destroyed" << endl; }
	virtual void show() = 0;
	virtual void hide() = 0;
};

class Point : public Shape
{
protected:
	int x;
	int y;
public:
	Point(int _x = 0, int _y = 0) : x(_x), y(_y)
	{
		cout << "Point was created" << endl;
	}
	~Point() { cout << "Point was destroyed" << endl; }
	void show()
	{
		cout << "Point is shown at (";
		cout << x << "," << y << ")" << endl;
	}
	void hide()
	{
		cout << "Point is hidden at (";
		cout << x << "," << y << ")" << endl;
	}
	void move(int dx = 0, int dy = 0)
	{
		x += dx;
		y += dy;
	}
};

class Circle : virtual public Point
{
protected:
	int radius;
public:
	Circle(int _x = 0, int _y = 0, int rad = 1) : Point(_x, _y),
												radius(rad)
	{
		cout << "Circle was created" << endl;
	}
	void show(){
		cout << "Circle is shown at (";
		cout << x << "," << y << "," << radius << ")" << endl;
	}
	//void move(int dx, int dy) override
	//{

	//}
	void hide(){
		cout << "Circle is hidden at (";
		cout << x << "," << y << "," << radius << ")" << endl;
	}
	~Circle() { cout << "Circle was destroyed" << endl; }
};

class Rectangle : virtual public Point
{
protected:
	int width;
	int height;
public:
	Rectangle(int _x = 0, int _y = 0, int w = 1, int h = 1) : Point(_x, _y),
														width(w),
														height(h)
	{
		cout << "Rectangle was created" << endl;
	}
	void show() {
		cout << "Rectangle is shown at (";
		cout << x << "," << y << "," << width << "," << height << ")" << endl;
	}
	void hide() {
		cout << "Rectangle is hidden at (";
		cout << x << "," << y << "," << width << "," << height << ")" << endl;
	}
	~Rectangle() { cout << "Rectangle was destroyed" << endl; }
};

class CircleInRectangle : public Circle, public Rectangle
{
public:
	CircleInRectangle(int x = 0, int y = 0, int r = 1) :
												//Point(x,y),
												Circle(x,y,r),
												Rectangle(x,y,r,r)
	{
		
	}
	void show() 
	{ 
		Circle::show(); 
		Rectangle::show();
		move();
	}
	void hide()
	{
		Circle::hide();
		Rectangle::hide();
	}
};

int main()
{
	//Shape* shape = new Shape();
	Point* point = new Point();
	Circle* circle = new Circle(2, 3, 5);
	Rectangle* rectangle = new Rectangle();

	const int shapesLength = 3;
	Shape* shapes[shapesLength];
	shapes[0] = new Circle();
	shapes[1] = new Point();
	shapes[2] = new Rectangle();
	for (int i = 0; i < shapesLength; ++i)
	{
		//Point* objAsPoint = (Point*)shapes[i];
		Point* objAsPoint = dynamic_cast<Point*>(shapes[i]);
		if (objAsPoint != nullptr)
			objAsPoint->move(10, 10);
		shapes[i]->show();
	}

	//delete shape;
	delete point;
	delete circle;
	delete rectangle;
	for (int i = 0; i < shapesLength; ++i)
		delete shapes[i];

	system("pause");
	return 0;
}