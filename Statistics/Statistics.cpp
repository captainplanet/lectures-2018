// Statistics.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <time.h>
#include <math.h>
using namespace std;

int main()
{	
	//"True" randomization
	srand(time(NULL));
	//Variables block
	const int size = 1e5; // 1 * 10^5
	int rand_array[size];
	double mean = 0;
	double dispersion = 0;
	//Array initialization
	for (int idx = 0; idx < size; ++idx)
		rand_array[idx] = rand() % 10;

	//Calculations
	//Mean value
	for (int idx = 0; idx < size; ++idx)
		mean += rand_array[idx];
	mean /= size;

	//Dispersion
	for (int idx = 0; idx < size; ++idx)
	{
		//dispersion += (mean - rand_array[idx])*(mean - rand_array[idx]);
		dispersion += pow(mean - rand_array[idx], 2);
	}
	
	dispersion /= size;

	//Output block
	cout << "MEAN VALUE: " << mean << endl;
	cout << "DISPERSION VALUE: " << dispersion << endl;

	//for (int idx = 0; idx < size; ++idx)
	//{
	//	cout << "rand_array[" << idx << "] = ";
	//	cout << rand_array[idx] << endl;
	//}

	system("pause");
    return 0;
}

